module lis.versionspec;

import std.algorithm.comparison;
import std.algorithm.iteration;
import std.algorithm.searching;
import std.array;
import std.conv;
import std.range;
import std.regex;

/**
	Basic struct for storing a version.
	It can be compared to other versions using simple operators "<" and ">".
 */
shared struct Version
{
	private string rawString;
	private ushort[] digits;
	
	/// Constructs the object from a string with dot-separated digits.
	this(in string rawString) {
		try {
			this.rawString = rawString;
			foreach (string part; rawString.splitter("."))
				digits ~= part.to!ushort;
		}
		catch (ConvException e) {
			throw new InvalidVersionString(rawString);
		}
	}
	
	/// Returns the string that represents the version.
	string toString() const {
		return digits.length ? digits.to!(string[]).join(".") : rawString;
	}
	
	/**
		Returns -1, 0 or 1 depending on whether version one is less, equal or greater than version two.
	 */
	int opCmp(in Version that) const {
		if (!this.digits.length || !that.digits.length)
			return cmp(this.rawString, that.rawString);
		
		if (this.digits.length < that.digits.length && equal(this.digits, that.digits[0..this.digits.length]))
			return -1;
		if (this.digits.length > that.digits.length && equal(this.digits[0..that.digits.length], that.digits))
			return 1;
		
		foreach (i; 0..this.digits.length) {
			if (i >= that.digits.length)
				return 1;
			
			if (this.digits[i] < that.digits[i])
				return -1;
			if (this.digits[i] > that.digits[i])
				return 1;
		}
		
		return 0;
	}
	
	short opEqual(in Version other) const {
		return equal(this.digits, other.digits);
	}
}

unittest {
	void testOrder(in string one, in string two) {
		assert(shared Version(one) <  shared Version(two), "Comparison test failed: \""~one~"\" < \""~two~"\".");
		assert(shared Version(two) >  shared Version(one), "Comparison test failed: \""~two~"\" > \""~one~"\".");
		assert(shared Version(one) == shared Version(one), "Comparison test failed: \""~one~"\" == \""~one~"\".");
		assert(shared Version(two) == shared Version(two), "Comparison test failed: \""~two~"\" == \""~two~"\".");
	}
	
	testOrder("1.2.3", "1.2.4");
	testOrder("1.2.3", "1.2.3.1");
	testOrder("1.2.3", "5.2.3");
	testOrder("1.1",   "1.2.3");
}

/**
	Interface which every version specification implements.
	A version specification is an expression which defines which version(s) are ok.
 */
shared interface VersionSpec
{
	bool matches(in string vers) const;
	
	/**
		Parses given string according to dub's description of possible formats,
		see http://code.dlang.org/package-format?lang=json#version-specs.
	 */
	static VersionSpec fromString(in string str) {
		if (str == "*")
			return new VersionSpec_AnyVersion();
		
		else if (auto m = str.matchFirst(ctRegex!`^==(\d+(?:\.\d+)*)$`))
			return new VersionSpec_CertainVersion(m.captures[1]);
		
		else if (auto m = str.matchFirst(ctRegex!`^~>(\d+(?:\.\d+)*)$`))
			return new VersionSpec_AutomaticRange(m.captures[1]);
		
		else if (auto m = str.matchFirst(ctRegex!`^>=(\d+(?:\.\d+)*)$`))
			return new VersionSpec_MinimumVersion(m.captures[1]);
		
		else if (auto m = str.matchFirst(ctRegex!`^>=(\d+(?:\.\d+)*)\s<(\d+(?:\.\d+)*)$`))
			return new VersionSpec_CertainRange(m.captures[1], m.captures[2]);
		
		else if (auto m = str.matchFirst(ctRegex!`^~(\S+)$`))
			return new VersionSpec_GitBranch(m.captures[1]);
		
		else
			throw new InvalidVersionString(str);
	}
}

/**
	A version specification that matches any version.
 */
class VersionSpec_AnyVersion : VersionSpec
{
	bool matches(in string vers) shared const {
		return true;
	}
}

/**
	A version specification that matches only given version.
 */
class VersionSpec_CertainVersion : VersionSpec
{
	protected Version vers;
	
	this(in string versionString) {
		vers = shared Version(versionString);
	}
	
	bool matches(in string vers) shared const {
		try {
			return shared Version(vers) == this.vers;
		}
		catch (InvalidVersionString e) {
			return false;
		}
	}
}

/**
	A version specification that matches any version higher than given one.
 */
class VersionSpec_MinimumVersion : VersionSpec
{
	protected Version vers;
	
	this(in string versionString) {
		vers = shared Version(versionString);
	}
	
	bool matches(in string versionString) shared const {
		try {
			return shared Version(versionString) >= this.vers;
		}
		catch (InvalidVersionString e) {
			return false;
		}
	}
}

/**
	A version specification that matches between given two versions (including the first one).
 */
class VersionSpec_CertainRange : VersionSpec
{
	protected shared Version vers1;
	protected shared Version vers2;
	
	this(in string versionString1, in string versionString2) {
		vers1 = shared Version(versionString1);
		vers2 = shared Version(versionString2);
	}
	
	bool matches(in string vers) shared const {
		shared Version otherVersion = vers;
		return this.vers1 <= otherVersion && otherVersion < this.vers2;
	}
}

/**
	Works exactly like CertainRange, except second margin is chosen automatically
	by increasing second-last digit in the first margin version and resetting the last digit.
	E.g. "1.2.3" means ">=1.2.3 <1.3.0".
 */
class VersionSpec_AutomaticRange : VersionSpec
{
	protected shared Version vers1;
	protected shared Version vers2;
	
	this(in string versionString) {
		vers1 = shared Version(versionString);
		
		vers2 = versionString.dup;
		vers2.digits[$-2] = vers2.digits[$-2];
		vers2.digits[$-1] = 0;
	}
	
	bool matches(in string vers) shared const {
		shared Version otherVersion = vers;
		return this.vers1 <= otherVersion && otherVersion < this.vers2;
	}
}

/**
	A version specification that defines using git branch name instead of a version.
 */
class VersionSpec_GitBranch : VersionSpec
{
	protected string branch;
	
	this (in string branch) {
		this.branch = branch;
	}
	
	bool matches(in string vers) shared const {
		try {
			return vers == "~"~this.branch;
		}
		catch (InvalidVersionString e) {
			return false;
		}
	}
}

class InvalidVersionString : Exception {
	this(in string versionString) {
		super("Invalid version specification: \""~versionString~"\".");
	}
}