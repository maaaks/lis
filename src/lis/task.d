module lis.task;

import lis.tree;
import std.algorithm.searching;
import std.concurrency;

/**
	A single node of the tasks tree.
	It has both subtasks (references to one or more tasks that are required for this task)
	and supertasks (references to one or more tasks that require this task).
	A subtasks is considered done when all its subtasks are done.
	
	The life cycle of a task is following:
	 1. A task is created. Its constructor should NOT do much and take long.
	 2. Once run() is called for the first time (with null as argument),
	 	the first part of the task is being done. Usually it includes creating one or more subtasks.
	 3. If the task has some undone subtasks, they all are being performed in parallel.
	 	After a subtask is considered done, run(subtask) is called.
	 	The method can generate new subtasks if neccessary, they will also be added to execution queue.
	 4. After all subtasks are done, the whole task is considered done, too,
	 	and this is being reported to all its supertasks (by calling their run(subtask)).
	 	If the Task is a DisposableTask, it is automatically removed at this point.
	 	
	The status of the task (undone/done) is stored in a private field
	and is being accessed only by Tree.run().
 */
shared class Task
{
	enum Status {
		NotStarted, /// Still has not been enqueued
		InProgress, /// Has been enqueued or even finished, but still has unfinished subtasks
		Finished,   /// Is finished, and all its subtasks are already finished
	}
	
	package Task[] supertasks; /// Tasks that are required to be done by this task
	package Task[] subtasks;   /// Tasks that require this task to be done
	package Status status;     /// Whether or not the task is finished or not
	
	/// Adds a new subtask for current task
	final void addSubtask(shared(Task) subtask) {
		if (!subtasks.canFind!((a, b) => cast(Task) a == cast(Task) b)(subtask)) {
			this.subtasks ~= subtask;
			subtask.supertasks ~= this;
		}
	}
	
	/// Callback that is called before running subtasks (with null as the argument)
	/// and later every time any subtask is finished (with that subtask as the argument)
	abstract void run(shared(Task) subtask);
}

/**
	The same as Task, but the object will be deleted
	after it is done (and its subtasks are done) and after this is reported to supertask.
 */
class DisposableTask : Task
{
}