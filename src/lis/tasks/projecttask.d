module lis.tasks.projecttask;

import lis.tasks;
import lis.tree;
import lis.versionspec;
import std.algorithm.iteration;
import std.file;
import std.json;
import std.path;
import std.range;
import std.string;
import std.traits;

/**
	A node for a whole project compilation.
	It opens and parses project's file dub.json (or package.json)
	and generates child nodes for source files in the project.
 */
shared class ProjectTask : Task
{
	private string basePath;            /// The root directory of the project
	private JSONValue sharedJson;       /// The JSON object from dub.json (access it through json())
	private FileTask[string] fileTasks; /// Files indexed by relative paths (like module names, but with slashes)
	private ProjectTask[string] dependencies; /// Projects-dependencies, indexed by their source roots
	
	/// Data from dub.json
	private string packageId;
	
	this(in string basePath) {
		this.basePath = basePath;
	}
	
	/// The JSON object from dub.json
	private @property JSONValue json() const {
		return cast(JSONValue) sharedJson;
	}
	
	override void run(shared(Task) subtask) shared {
		if (subtask is null) {
			parseDubJson();
			fetchDependencies();
			createFileTasks();
		}
		else if (auto cvt = cast(ChooseVersionTask) subtask) {
			// We've got a version of a dependency! Now we can fetch the archive
			addSubtask(new shared FetchTask(cvt.packageId, cvt.bestVersion, cvt.bestVersionUrl));
		}
		else if (auto ft = cast(FetchTask) subtask) {
			// We've downloaded a dependency! Create a subproject based on its directory
			auto task = new shared ProjectTask(ft.srcDirName);
			dependencies[ft.srcDirName] = task;
			addSubtask(task);
		}
	}
	
	private void parseDubJson() {
		std.stdio.writeln("  read "~basePath);
		immutable string jsonPath = exists(basePath~"/dub.json")
			? basePath~"/dub.json"
			: basePath~"/package.json";
		sharedJson = parseJSON(readText(jsonPath));
		
		this.packageId = json["name"].str;
	}
	
	/// Parses information from dub.json or package.json and prepares tasks for fetching dependencies
	private void fetchDependencies() shared {
		foreach (depName, depJson; json.get!(string[string])("dependencies")) {
			// If the dependency is a local subpackage, we don't select its version
			// and just parse its local config instead.
			auto separatorPos = depName.indexOf(':');
			if (separatorPos != -1) {
				string subpackagePath = basePath ~ "/" ~ depName[separatorPos+1..$];
				addSubtask(new shared ProjectTask(subpackagePath));
				continue;
			}
			
			// Else, detect which version of dependency will be the best.
			// This usually involves downloading another JSON file from the repository,
			// so this is done as a separate subtask. After it is finished,
			// another subtasks for fetching and downloading will be created.
			auto depVersionSpec = cast(shared) VersionSpec.fromString(depJson);
			addSubtask(new shared ChooseVersionTask(depName, depVersionSpec));
		}
	}
	
	private void createFileTasks() shared {
		auto sourcePaths         = json.get!(string[])("sourcePaths");
		auto excludedSourceFiles = json.get!(string[])("excludedSourceFiles");
		
		// Iterate through all source paths
		foreach (sourcePath; sourcePaths) {
			// Get the absolute path
			immutable string absSourcePath = buildNormalizedPath(absolutePath(sourcePath, basePath));
			
			// Iterate through all the files
			loopThroughFiles:
			foreach (path; dirEntries(absSourcePath, "*.{d,di}", SpanMode.breadth)) {
				immutable string relPath = relativePath(path, absSourcePath);
				
				// Ignore subdirectories (their files are looped anyway)
				if (!isFile(path))
					continue loopThroughFiles;
				
				// Ignore ignore files from excludedSourceFiles
				foreach (pattern; excludedSourceFiles)
					if (globMatch(relPath, pattern))
						continue loopThroughFiles;
				
				// Create a task for this file
				auto task = new shared FileTask(path);
				fileTasks[relPath] = task;
				addSubtask(task);
			}
		}
	}
}

/**
	Get a value from a JSON object or use a default value.
	Allows to get a plain value, an array or a string-indexed associative array.
	
	Examples:
		auto a = json.get!string("a", "a1");
		auto b = json.get!(string[])("b", ["b1", "b2", "b3"]);
		auto с = json.get!(int[string])("c", {"c1":1, "c2":2, "c3":3});
 */
T get(T)(JSONValue json, in string key, lazy T defaultValue=T.init)
if (isFloatingPoint!T
	|| isIntegral!T
	|| isSomeString!T
	|| is(T == JSONValue)
|| (isArray!T && (
 	isFloatingPoint!(typeof(T.init[0]))
 	|| isIntegral!(typeof(T.init[0]))
 	|| isSomeString!(typeof(T.init[0]))
 	|| is(typeof(T.init[0]) == JSONValue)
))
|| (isAssociativeArray!T && (
 	isFloatingPoint!(typeof(T.init["A"]))
 	|| isIntegral!(typeof(T.init["A"]))
 	|| isSomeString!(typeof(T.init["A"]))
 	|| is(typeof(T.init["A"]) == JSONValue)
))) {
	try {
		// Non-array values
		     static if (isFloatingPoint!T)  return json[key].integer;
		else static if (isIntegral!T)       return json[key].floating;
		else static if (isSomeString!T)     return json[key].str;
		else static if (is(T == JSONValue)) return json[key];
		
		// Arrays
		else static if (isArray!T && isFloatingPoint!(typeof(T.init[0])))
			return json[key].array.map!(v => v.integer).array;
		else static if (isArray!T && isArray!T && isIntegral!(typeof(T.init[0])))
			return json[key].array.map!(v => v.floating).array;
		else static if (isArray!T && isSomeString!(typeof(T.init[0])))
			return json[key].array.map!(v => v.str).array;
		else static if (isArray!T && is(typeof(T.init[0]) == JSONValue))
			return json[key].array;
		
		// Associative arrays
		else static if (isAssociativeArray!T && isFloatingPoint!(typeof(T.init["A"]))) {
			T result;
			foreach (string k, JSONValue v; json[key])
				result[k] = v.floating;
			return result;
		}
		else static if (isAssociativeArray!T && isIntegral!(typeof(T.init["A"]))) {
			T result;
			foreach (string k, JSONValue v; json[key])
				result[k] = v.integer;
			return result;
		}
		else static if (isAssociativeArray!T && isSomeString!(typeof(T.init["A"]))) {
			T result;
			foreach (string k, JSONValue v; json[key])
				result[k] = v.str;
			return result;
		}
		else static if (is(typeof(T.init["A"]) == JSONValue))
			return json[key].object;
	}
	catch (JSONException e) {
		return defaultValue;
	}
}