module lis.tasks.chooseversiontask;

import lis.tasks;
import lis.tree;
import lis.versionspec;
import std.algorithm.iteration;
import std.array;
import std.json;
import std.net.curl;
import std.path;

shared class ChooseVersionTask : DisposableTask
{
	immutable string packageId;
	private const VersionSpec versSpec;
	
	/// Results will be saved here
	package shared(Version) bestVersion;
	package string bestVersionUrl;
	
	this(in string packageId, shared(VersionSpec) versSpec) {
		this.packageId = packageId;
		this.versSpec = versSpec;
	}
	
	override void run(shared(Task) subtask) shared {
		// Get the JSON about the package
		std.stdio.writeln("  get "~"http://code.dlang.org/packages/"~packageId~".json");
		JSONValue json = parseJSON(get("http://code.dlang.org/packages/"~packageId~".json"));
		
		// Find all versions that match the specification
		JSONValue[] versions = json["versions"].array();
		versions = versions.filter!(vers => versSpec.matches(vers["version"].str)).array();
		
		// Select the most recent version
		
		foreach (JSONValue currVersionJson; versions) {
			if (bestVersionUrl is null) {
				bestVersion = cast(shared(Version)) currVersionJson["version"].str;
				bestVersionUrl = currVersionJson["downloadUrl"].str;
			}
			else {
				auto currVersion = cast(shared(Version)) currVersionJson["version"].str;
				if (currVersion > bestVersion) {
					bestVersion = currVersion;
					bestVersionUrl = currVersionJson["downloadUrl"].str;
				}
			}
		}
	}
}