module lis.tasks.fetcharchivetask;

import lis.tasks;
import lis.tree;
import lis.versionspec;
import std.array;
import std.file;
import std.net.curl;
import std.path;
import std.regex;
import std.stdio;
import std.zip;

class FetchTask : DisposableTask
{
	immutable string packageId; /// Name of the project to download
	Version vers;               /// The exact version to download
	immutable string url;       /// URL to fetch the version from
	string srcDirName;
	string archiveName;
	
	this(in string packageId, Version vers, in string url) {
		this.packageId = packageId;
		this.vers = vers;
		this.url = url;
	}
	
	override void run(shared(Task) subtask) shared {
		mkdirRecurse(expandTilde("~/.lis/archives/"~packageId));
		srcDirName = expandTilde("~/.lis/packages/"~packageId~"/"~vers.toString());
		archiveName = expandTilde("~/.lis/archives/"~packageId~"/"~vers.toString()~url.extension());
		
		// Do nothing if source files already exist
		if (exists(srcDirName) && dirEntries(srcDirName, SpanMode.shallow).array.length > 0) return;
		
		// Download ZIP into ~/.lis/archives if it is not yet downloaded
		if (!exists(archiveName)) {
			writeln("  get "~url);
			download(url, archiveName);
		}
		
		// Unzip the archive
		writeln("  unzip "~packageId~" "~vers.toString());
		auto zip = new ZipArchive(read(archiveName));
		foreach (ArchiveMember member; zip.directory) {
			// Omit first-level directory in all paths
			immutable string subpath = member.name.replace(ctRegex!`^[^/]+/`, "/");
			
			if (subpath[$-1] == '/') {
				// It's a directory
				mkdirRecurse(srcDirName~subpath);
			}
			else {
				// It's a file
				mkdirRecurse(buildNormalizedPath(absolutePath("..", srcDirName~"/"~subpath)));
				std.file.write(srcDirName~subpath, zip.expand(member));
			}
		}
	}
}