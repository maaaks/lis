module lis.tasks.filetask;

import lis.tasks;
import lis.tree;
import std.algorithm.iteration;
import std.d.ast;
import std.d.lexer;
import std.d.parser;
import std.file;
import std.range;

shared class FileTask : Task
{
	immutable string path;
	string[] imports;
	
	this(in string path) {
		this.path = path;
		
		// Parse file with libdparse
		LexerConfig lexerConfig = {
			fileName: path,
			stringBehavior: StringBehavior.source,
			whitespaceBehavior: WhitespaceBehavior.skip,
		};
		StringCache cache = StringCache(StringCache.defaultBucketCount);
		auto tokens = getTokensForParser(cast(ubyte[]) read(path), lexerConfig, &cache);
		auto m = parseModule(tokens, path, null, &_messageFunction);
		
		// Find all the imports
		auto importFinder = new ImportFinder(this);
		m.accept(importFinder);
	}
	
	override void run(shared(Task) subtask) shared {
		
	}

	/// A dummy callback for libdparse's parseModule().
	private static void _messageFunction(string, size_t, size_t, string, bool) {
	}
}

/// An aux visitor class for collecting information about imports from a file.
class ImportFinder : ASTVisitor
{
	shared(FileTask) task;
	
	alias visit = ASTVisitor.visit;
	
	this(shared(FileTask) task) {
		this.task = task;
	}
	
	override void visit(const SingleImport singleImport) {
		task.imports ~= singleImport.identifierChain.identifiers.map!(i => i.text).array.join(".");
	}
}