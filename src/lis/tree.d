module lis.tree;

import lis.task;
import std.algorithm.comparison;
import std.algorithm.searching;
import std.concurrency;

struct QueueItem
{
	shared(Task) task;
	shared(Task) subtask;
}

void treeRunner(shared(Task) rootTask, uint poolSize) {
	Tid[] freeWorkers = new Tid[poolSize];
	QueueItem[] queue;
	
	// Create pool of workers
	foreach (i; 0..poolSize)
		freeWorkers[i] = spawn(&worker);
		
	// Add the first task to queue
	queue ~= QueueItem(rootTask);
	rootTask.status = Task.Status.InProgress;
	
	while (true) {
		// To give a worker a task, we obvously need a worker and a task.
		// If we don't have something, we need to wait until some worker will return a task to us,
		// so that the situation could change.
		if (freeWorkers.length == 0 || queue.length == 0) {
			// Since we don't have a free worker, we have to wait for another work to be finished.
			// And since another work is finished, we need to update some tasks' statuses
			// and to inform supertasks about it.
			auto msg = receiveOnly!(Tid, shared(Task), shared(Task));
			auto tid         = msg[0];
			auto prevTask    = msg[1];
			auto prevSubtask = msg[2];
			
			// A performed task is finished when it does not have any unfinished subtasks
			// If unfinished subtasks still exist, enqueue each of them
			// (unless some of them have already been performed or enqueued)
			bool hasUnfinishedTasks = false;
			foreach (subtask; prevTask.subtasks) {
				// If we see a new task, enqueue it
				if (subtask.status == Task.Status.NotStarted) {
					queue ~= QueueItem(subtask);
					subtask.status = Task.Status.InProgress;
				}
				
				// Also use this loop to remember whether any of subtasks are unfinished
				if (subtask.status != Task.Status.Finished)
					hasUnfinishedTasks = true;
			}
			
			// If the task was runned because of a disposable subtask, we can remove the subtask
			// because no one will ever need it again
			if (cast(DisposableTask) prevSubtask) {
				auto i = prevTask.subtasks.countUntil!((a, b) => cast(Task) a == cast(Task) b)(prevSubtask);
				prevTask.subtasks = prevTask.subtasks[0..i] ~ prevTask.subtasks[i+1..$];
			}
			
			// If we have not found any unfinished subtasks, mark the task finished
			if (!hasUnfinishedTasks) {
				prevTask.status = Task.Status.Finished;
				
				// If we have just finished the root task, then we are done
				if (cast(Task) prevTask == cast(Task) rootTask)
					return;
				
				// Else, all supertasks of this task should be informed that this task is now done
				foreach (supertask; prevTask.supertasks)
					queue ~= QueueItem(supertask, prevTask);
			}
			
			// Finally, add Tid to the pool of free workers
			freeWorkers ~= tid;
		}
		
		// Now, when we have a non-empty tasks queue and non-empty freeWorkers array,
		// let's take something from queue and give it to a worker.
		// Repeat it as many times as we can.
		auto count = min(freeWorkers.length, queue.length);
		foreach (i; 0..count)
			freeWorkers[i].send(queue[i].task, queue[i].subtask);
		
		// Remove the workers and queue items we just used
		freeWorkers = freeWorkers[count..$];
		queue = queue[count..$];
	}
}

/**
	This function repeatedly gets a new task from parent thread and executes it.
 */
package void worker() {
	bool running = true;
	while (running) {
		receive(
			(shared(Task) task, shared(Task) subtask) {
				try {
					task.run(subtask);
					ownerTid.send(thisTid, task, subtask);
				}
				catch(shared(Exception) e) {
					ownerTid.send(e);
				}
			},
			(OwnerTerminated e) {
				// The parent thread has stopped, wee need to stop too
				running = false;
			},
		);
	}
}