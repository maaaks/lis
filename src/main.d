import core.cpuid;
import lis.tasks;
import lis.tree;
import std.conv;
import std.getopt;
import std.stdio;

void main(string[] args) {
	// Parse optional arguments
	uint numOfThreads = threadsPerCPU();
	getopt(args,
		"j", &numOfThreads);
	
	// After optional arguments are gone, we can get project path to work wiith
	if (args.length != 2)
		throw new InvalidArgumentsList(args.length-1);
	immutable string projectPath = args[1];
	
	auto rootTask = new shared ProjectTask(projectPath);
	treeRunner(rootTask, numOfThreads);
}

class InvalidArgumentsList : Exception {
	this(in ulong count) {
		super("Got "~count.to!string~" unnamed arguments, expected 1.");
	}
}